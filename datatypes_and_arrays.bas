REM  *****  BASIC  *****
Option Explicit

Sub Main
print "hello world"
Dim MyInt%, MyInt2 as Integer ' 2 ways to declare integer variables
Dim MyStr$, MyStr2 As String ' 2 ways to declare string variables
MyStr = " this is test"
MyStr2 = "this string will span " &_
         "multiple lines; " &_
         "seriously, it doesn't stop at 2!"
print MyStr2
Dim MyLong&, MyLong2 as Long ' 2 ways to declare long variables
Dim MySingle!, MySingle2 as Single ' 2 ways to declare single (as opposed to Double)
Dim MyDouble#, MyDouble2 as Double ' "" "" double
Dim MyCurr@, MyCurr2 as Currency   ' "" "" currency
' Dim MyFloat as Float : there is no 'Float' type in LibreOffice basic; use Single or Double
Dim MyBoolean as Boolean ' boolean
Dim MyDate as Date ' date
dim var1 as String
var1 = "not a number"
print "here is your number:"; var1
var1 = 12
print "here is your number again: "; var1
End Sub

Sub Rounding
Dim A as Integer, B As Integer, C as Double
A = 1223.53 ' is rounded
B = - 23446.46 ' is rounded
C = + 3532.76323 ' guess what? not rounded
print "here is A: ";A
print "here is B: ";B
End Sub

Sub Constants
Const ConstantlyConsting="Better than french toast"
print ConstantlyConsting
End Sub

Sub Arrays
Dim MyArray(3) ' declares array variable with 4 slots (highest index of 3)
Dim MyIntegers(5) as Integer ' declares array variable with 6 slots
Dim OtherIntegers(5 To 10) as Integer ' declares integer array with indexes 5 to 10
Dim NegIntegers(-5 To 10) as Integer ' yes, negative indexes too **palm to forehead**
MyArray(0) = 20
MyArray(1) = 21
MyArray(2) = -15
MyArray(3) = 127
Print( LBound(Myarray, 2) )
Print( UBound(Myarray, 2) )
End Sub

Sub Branching
	Dim A, B, C
	A = 5 : B = 12 : C = -2
	If A > B Then
		Print( "A is greater than B: " & A & ", " & B )
	ElseIf C < B Then
		Print( "C is less than B: " & C & ", " & B )
	Else
		Print( "Forget about it!" )
	End If
End Sub

Sub ForLoop
Dim I
For I = 1 To 10
Print(I)
If I > 5 Then
	Exit For ' Conditional exit
End If
Next I
End Sub

Sub DoWhile
Dim I as Integer
I = -2
Do Until I > 10
Print(I)
I = I + 1
Loop
End Sub

Sub WhileWend
Dim I as Integer
	I = 12
	While I > 0
		Print(I)
		I = I - 1
	Wend
End Sub

Function IAmARobot
IAmARobot = "this is a return value" ' return value given by assigning function name to return value
Print("You'll be surprised by what is about to happen") ' keep coding
End Function ' returns "this is a return value" but doesn't exit early

SUB SOMEOTHERSUBROUTINE
Dim Foo$
Foo = IAmARobot
Print("Here it finally is: " & Foo)
END SUB

SUB SORT
	DIM ENTRY(1 TO 10) AS STRING
	DIM COUNT AS INTEGER
	DIM COUNT2 AS INTEGER
	DIM TEMP AS STRING
	
	ENTRY(1) = "Patty"
    ENTRY(2) = "Kurt"
    ENTRY(3) = "Thomas"
    ENTRY(4) = "Michael"
    ENTRY(5) = "David"
    ENTRY(6) = "Cathy"
    ENTRY(7) = "Susie"
    ENTRY(8) = "Edward"
    ENTRY(9) = "Christine"
    ENTRY(10) = "Jerry"
	
	FOR COUNT = 1 TO 9
		FOR COUNT2 = COUNT + 1 TO 10
			IF ENTRY(COUNT) > ENTRY(COUNT2) THEN
				TEMP = ENTRY(COUNT)
				ENTRY(COUNT) = ENTRY(COUNT2)
				ENTRY(COUNT2) = TEMP
			END IF
		NEXT COUNT2
	NEXT COUNT
	
	FOR COUNT = 1 TO 10
		PRINT ENTRY(COUNT)
	NEXT COUNT
END SUB