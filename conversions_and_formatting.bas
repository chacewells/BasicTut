REM  *****  CONVERSION FUNCTIONS  *****

Sub Main
Print("Let's do some conversions!")
Dim Var ' declaration
Var = 25 ' this should turn into any data type
Print("String: " & CStr(Var))
Print("Int: " & CInt(Var))
Print("Long: " & CLng(Var))
Print("Single: " & CSng(Var))
Print("Double: " & CDbl(Var))
Print("Boolean: " & CBool(Var))
Print("Date: " & CDate(Var))

Dim NoLocale
NoLocale = "15.225"
Dim A, B
A = 1.025
B = A + Val(NoLocale) ' Does number conversion and always expects "." to be a decimal
Print(NoLocale & " + " & A & " = " & B)
End Sub

Sub Formatting
MyFormat = "0.00"
Values = Array(3)
Values(0) = -1579.8
Values(1) = 1579.8
Values(2) = 0.4
Values(3) = 0.434
For I = 0 To 3
MyVal = Array(I)
MyString = Format(MyVal, MyFormat)
Print(MyString)
Next I

MyString = Format(-1579.8, MyFormat)     ' Provides "-1579,80"
MyString = Format(1579.8, MyFormat)      ' Provides "1579,80"
MyString = Format(0.4, MyFormat)         ' Provides "0,40"
MyString = Format(0.434, MyFormat)       ' Provides "0,43"
End Sub