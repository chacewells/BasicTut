REM  *****  PROCEDURES  *****

Sub Main
Dim A As Integer : A = 10
Print("A is now " & A)
ChangeValue(A) ' passed by reference, so A changes
Print("A is still " & A)

Dim B, C
A = CalculateFactorial(5)
B = CalculateFactorial("elephant")
C = CalculateFactorial(-12)
Print(A)
Print(B)
Print(C)
End Sub

Sub ChangeValue(ByVal TheValue As Integer)
    Print("Inside ChangeValue, the value is first " & TheValue)
    TheValue = 20
    Print("Inside ChangeValue, the value has been changed to " & TheValue)
End Sub

Function CalculateFactorial( Number )
If Number < 0 Or Number <> Int(Number) Then
    CalculateFactorial = "Invalid number for factorial!"
ElseIf Number = 0 Then
    CalculateFactorial = 1
Else
    CalculateFactorial = Number * CalculateFactorial(Number - 1)
Endif
End Function
